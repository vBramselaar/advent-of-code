#!/usr/bin/env -S guile -e entrée -s
!#

(add-to-load-path (string-append (dirname (current-filename)) "/.."))

(use-modules (aoc)
			 (srfi srfi-1))

(define open-parens '(#\( #\[ #\{ #\<))
(define closing-parens '(#\) #\] #\} #\>))

(define (entrée args)
  (let ((data-list (process-input args process-line)))
	(unless (null? data-list)
	  (solve data-list part1 part2))))

(define (part1 data-list)
  (let loop ((remaining-lines data-list)
			 (score 0))
	(cond
	 ((null? remaining-lines)
	  score)
	 (else
	  (let ((result (create-tree (car remaining-lines))))
		(if (not (list? result))
			(loop (cdr remaining-lines) (+ score (get-paren-score result)))
			(loop (cdr remaining-lines) score)))))))

(define (create-tree line)
  (let loop ((remaining-line line)
			 (tree '()))
	(cond
	 ((null? remaining-line)
	  tree)
	 ((member (car remaining-line) open-parens)
	  (loop (cdr remaining-line) (append tree (list (car remaining-line)))))
	 ((member (car remaining-line) closing-parens)
	  (if (equal? (car remaining-line) (get-closing-paren (car (reverse tree))))
		  (loop (cdr remaining-line) (reverse (cdr (reverse tree))))
		  (car remaining-line))))))

(define (calculate-score parens)
  (let loop ((remaining-parens (reverse parens))
			 (score 0))
	(cond
	 ((null? remaining-parens)
	  score)
	 (else
	  (loop (cdr remaining-parens) (+ (* 5 score) (get-paren-score2 (get-closing-paren (car remaining-parens)))))))))

(define (part2 data-list)
  (let loop ((remaining-lines data-list)
			 (score '()))
	(cond
	 ((null? remaining-lines)
	  (list-ref (sort-list score <) (inexact->exact (round (* 0.5 (length score))))))
	 (else
	  (let ((result (create-tree (car remaining-lines))))
		(if (list? result)
			(loop (cdr remaining-lines) (append score (list (calculate-score result))))
			(loop (cdr remaining-lines) score)))))))

(define (get-closing-paren paren)
  (cond
   ((eqv? paren #\()
	#\))
   ((eqv? paren #\[)
	#\])
   ((eqv? paren #\{)
	#\})
   ((eqv? paren #\<)
	#\>)))

(define (get-paren-score paren)
  (cond
   ((eqv? paren #\))
	3)
   ((eqv? paren #\])
	57)
   ((eqv? paren #\})
	1197)
   ((eqv? paren #\>)
	25137)))

(define (get-paren-score2 paren)
  (cond
   ((eqv? paren #\))
	1)
   ((eqv? paren #\])
	2)
   ((eqv? paren #\})
	3)
   ((eqv? paren #\>)
	4)))

(define (process-line line)
  (let* ((delimited-line (string->list line)))
	delimited-line))
