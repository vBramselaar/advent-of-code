#!/usr/bin/env -S guile -e entrée -s
!#

(add-to-load-path (string-append (dirname (current-filename)) "/.."))

(use-modules (aoc))

(define (entrée args)
  (let ((data-list (process-input args process-line)))
	(unless (null? data-list)
	  (solve data-list part1 part2))))

(define (part1 data-list)
  (let loop ((answer 0)
			 (number (car data-list))
			 (rest-of-list (cdr data-list)))
	(cond
	 ((null? rest-of-list)
	  answer)
	 ((> (car rest-of-list) number)
	  (loop (+ answer 1) (car rest-of-list) (cdr rest-of-list)))
	 (else
	  (loop answer (car rest-of-list) (cdr rest-of-list))))))

(define (part2 data-list)
  (let loop ((answer 0)
			 (number (sum-first-three data-list))
			 (rest-of-list (cdr data-list)))
	(cond
	 ((< (length rest-of-list) 3)
	  answer)
	 ((< number (sum-first-three rest-of-list))
	  (loop (+ answer 1) (sum-first-three rest-of-list) (cdr rest-of-list)))
	 (else
	  (loop answer (sum-first-three rest-of-list) (cdr rest-of-list))))))

(define (process-line line)
  (string->number line))

(define (sum-first-three list)
  (+ (list-ref list 0)
	 (list-ref list 1)
	 (list-ref list 2)))
