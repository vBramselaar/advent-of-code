#!/usr/bin/env -S guile -e entrée -s
!#

(add-to-load-path (string-append (dirname (current-filename)) "/.."))

(use-modules (aoc)
			 (ice-9 rdelim))

(define (entrée args)
  (let ((data-list (process-input-port args process-port)))
	(unless (null? data-list)
	  (solve data-list part1 part2))))

(define (part1 data-list)
  (let ((balls (car data-list))
		(sheets (cdr data-list)))
	(let iterate-balls ((to-be-drawn (cdr balls))
						(drawn-balls (list (car balls))))
	  (let ((result (check-sheets sheets drawn-balls)))
		(if (null? result)
			(iterate-balls (cdr to-be-drawn) (append (list (car to-be-drawn)) drawn-balls))
			(* (count-unmarked (car result) drawn-balls) (car drawn-balls)))))))

(define (check-sheets sheets drawn-balls)
  (let loop ((remaining-sheets sheets)
			 (winning-sheets '()))
	(if (null? remaining-sheets)
		winning-sheets
		(let ((rows-result (check-rows (car remaining-sheets) drawn-balls))
			  (collumn-result (check-rows (apply map list (car remaining-sheets)) drawn-balls)))
		  (if (and (null? rows-result) (null? collumn-result))
			  (loop (cdr remaining-sheets) winning-sheets)
			  (loop (cdr remaining-sheets) (append winning-sheets (list (car remaining-sheets)))))))))

(define (check-rows sheet drawn-balls)
  (let iterate-rows ((rows sheet))
	(if (null? rows)
		'()
		(let ((none-matching (filter (lambda (e) (not (memv e drawn-balls))) (car rows))))
		  (if (null? none-matching)
			  (car rows)
			  (iterate-rows (cdr rows)))))))

(define (count-unmarked sheet marked)
  (let count-loop ((count 0)
				   (rows sheet))
	(if (null? rows)
		count
		(let ((none-matching (filter (lambda (e) (not (memv e marked))) (car rows))))
		  (count-loop (+ count (apply + none-matching)) (cdr rows))))))

(define (part2 data-list)
  (let ((balls (car data-list))
		(sheets (cdr data-list)))
	(let iterate-balls ((to-be-drawn (cdr balls))
						(drawn-balls (list (car balls)))
						(last-winner '())
						(last-winner-balls '())
						(remaining-sheets sheets))
	  (if (null? to-be-drawn)
		  (let ((result (check-sheets remaining-sheets drawn-balls)))
			(if (null? result)
				(* (count-unmarked last-winner last-winner-balls) (car last-winner-balls))
				(* (count-unmarked result drawn-balls) (car drawn-balls))))
		  (let ((result (check-sheets remaining-sheets drawn-balls)))
			(if (null? result)
				(iterate-balls (cdr to-be-drawn) (append (list (car to-be-drawn)) drawn-balls) last-winner last-winner-balls remaining-sheets)
				(iterate-balls (cdr to-be-drawn) (append (list (car to-be-drawn)) drawn-balls) (car (reverse result)) drawn-balls (filter (lambda (e) (not (member e result))) remaining-sheets))))))))

(define (process-port port)
  (let line-loop ((new-list (list (string-numbers->list-numbers (read-line port) #\,)))
				  (line (read-line port)))
	(cond
	 ((eof-object? line)
	  new-list)
	 ((string-null? line)
	  (line-loop (append new-list (list (process-sheet-data port))) (read-line port))))))

(define (process-sheet-data port)
  (let sheet-loop ((index 0)
				   (bingo-sheet '()))
	(if (= index 5)
		bingo-sheet
		(sheet-loop (+ index 1) (append bingo-sheet (list (string-numbers->list-numbers (read-line port) #\space)))))))
