#!/usr/bin/env -S guile -e entrée -s
!#

(add-to-load-path (string-append (dirname (current-filename)) "/.."))

(use-modules (aoc))

(define byte-length 0)
(define (mask blength)
  (- (expt 2 blength) 1))

(define (entrée args)
  (let ((data-list (process-input args process-line)))
	(unless (null? data-list)
	  (solve data-list part1 part2))))

(define (get-bit data-list index)
  (let loop ((bit-amount 0)
			 (bytes data-list))
	(cond
	 ((null? bytes)
	  (if (> bit-amount (- (length data-list) bit-amount))
		  (ash #b1 index)
		  0))
	 (else
	  (if (logbit? index (car bytes))
		  (loop (+ bit-amount 1) (cdr bytes))
		  (loop bit-amount (cdr bytes)))))))

(define (part1 data-list)
  (let loop ((n 0)
			 (byte 0))
	(if (>= n byte-length)
		(* byte (logand (lognot byte) (mask byte-length)))
		(loop (+ n 1) (logior byte (get-bit data-list n))))))

(define (get-bit-amount data-list index)
  (let loop ((bit-amount 0)
			 (bytes data-list))
	(cond
	 ((null? bytes)
	  (cons bit-amount (- (length data-list) bit-amount)))
	 (else
	  (if (logbit? index (car bytes))
		  (loop (+ bit-amount 1) (cdr bytes))
		  (loop bit-amount (cdr bytes)))))))

(define (get-common data-list index bit)
  (let loop ((bytes data-list)
			 (common-list '()))
	(cond
	 ((null? bytes)
	  common-list)
	 ((eqv? (logbit? index (car bytes)) bit)
	  (loop (cdr bytes) (append common-list (list (car bytes)))))
	 (else
	  (loop (cdr bytes) common-list)))))

(define (get-result data-list type)
  (let loop ((mc data-list)
			 (index (- byte-length 1)))
	(cond
	 ((= (length mc) 1)
	  (car mc))
	 ((> (length mc) 1)
	  (let ((bit-pair (get-bit-amount mc index)))
		(cond
		 ((or (> (car bit-pair) (cdr bit-pair)) (= (car bit-pair) (cdr bit-pair)))
		  (loop (get-common mc index type) (- index 1)))
		 (else
		  (loop (get-common mc index (not type)) (- index 1)))))))))

(define (part2 data-list)
  (let ((mc (get-result data-list #t))
		(lc (get-result data-list #f)))
	(list mc lc (* mc lc))))

(define (process-line line)
  (set! byte-length (string-length line))
  (let loop ((byte 0)
			 (chars (string-reverse line)))
	(cond
	 ((string-null? chars)
	  byte)
	 (else
	  (if (equal? (string-take chars 1) "1")
		  (loop (logior (ash #b1 (- (string-length line) (string-length chars))) byte) (string-drop chars 1))
		  (loop byte (string-drop chars 1)))))))
