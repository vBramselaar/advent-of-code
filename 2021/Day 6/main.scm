#!/usr/bin/env -S guile -e entrée -s
!#

(add-to-load-path (string-append (dirname (current-filename)) "/.."))

(use-modules (aoc)
			 (ice-9 rdelim))

(define (entrée args)
  (let ((data-list (process-input-port args process-line)))
	(unless (null? data-list)
	  (solve data-list part1 part2))))

(define (part1 data-list)
  (let day-loop ((day 0)
				 (count-list data-list))
	(cond
	 ((= day 80)
	  (apply + count-list))
	 (else
	  (day-loop (+ day 1) (process-day count-list))))))

(define (process-day fish-list)
  (let ((new-list (make-list 9 0)))
	(let loop ((index 0))
	  (cond
	   ((> index 8)
		new-list)
	   ((= index 0)
		(list-set! new-list 6 (+ (list-ref new-list 6) (list-ref fish-list index)))
		(list-set! new-list 8 (+ (list-ref new-list 8) (list-ref fish-list index)))
		(loop (+ index 1)))
	   (else
		(list-set! new-list (- index 1) (+ (list-ref new-list (- index 1)) (list-ref fish-list index)))
		(loop (+ index 1)))))))

(define (part2 data-list)
  (let day-loop ((day 0)
				 (count-list data-list))
	(cond
	 ((= day 256)
	  (apply + count-list))
	 (else
	  (day-loop (+ day 1) (process-day count-list))))))


(define (process-line port)
  (let ((count-list (make-list 9 0)))
	(let loop ((fish-list (string-numbers->list-numbers (read-line port) #\,)))
	  (cond
	   ((null? fish-list)
		count-list)
	   (else
		(list-set! count-list (car fish-list) (+ (list-ref count-list (car fish-list)) 1))
		(loop (cdr fish-list)))))))
