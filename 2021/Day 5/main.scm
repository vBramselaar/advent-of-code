#!/usr/bin/env -S guile -e entrée -s
!#

(add-to-load-path (string-append (dirname (current-filename)) "/.."))

(use-modules (aoc))

(define grid-size (expt 1000 2))

(define (entrée args)
  (let ((data-list (process-input args process-line)))
	(unless (null? data-list)
	  (solve data-list part1 part2))))

(define (part1 data-list)
  (let ((grid (make-hash-table grid-size)))
	(let iterate-coordinates ((coordinate-list data-list))
	  (cond
	   ((null? coordinate-list)
		(length (filter (lambda (e) (>= e 2)) (hash-map->list (lambda (key value) value) grid))))
	   (else
		(fill-in-coordinates grid (car coordinate-list))
		(iterate-coordinates (cdr coordinate-list)))))))

(define (fill-in-coordinates grid coordinates)
  (let ((x-amount (- (car (cdr coordinates)) (car (car coordinates))))
		(y-amount (- (cdr (cdr coordinates)) (cdr (car coordinates)))))
	(cond
	 ((and (not (= x-amount 0)) (not (= y-amount 0)))
	  grid)
	 (else
	  (let loop ((x-index 0)
				 (y-index 0))
		(cond
		 ((and (or (> (abs x-index) (abs x-amount)) (zero? x-amount)) (or (> (abs y-index) (abs y-amount)) (zero? y-amount)))
		  grid)
		 (else
		  (increase-coordinate grid (cons (+ (car (car coordinates)) x-index) (+ (cdr (car coordinates)) y-index)))
		  (loop (+ x-index (divide-default-zero x-amount (abs x-amount))) (+ y-index (divide-default-zero y-amount (abs y-amount)))))))))))

(define (increase-coordinate grid coordinate)
  (let* ((value (hash-ref grid coordinate 0)))
	(hash-set! grid coordinate (+ value 1))))

(define (divide-default-zero a b)
  (if (zero? b)
	  0
	  (/ a b)))

(define (part2 data-list)
  (let ((grid (make-hash-table grid-size)))
	(let iterate-coordinates ((coordinate-list data-list))
	  (cond
	   ((null? coordinate-list)
		(length (filter (lambda (e) (>= e 2)) (hash-map->list (lambda (key value) value) grid))))
	   (else
		(fill-in-coordinates2 grid (car coordinate-list))
		(iterate-coordinates (cdr coordinate-list)))))))

(define (fill-in-coordinates2 grid coordinates)
  (let ((x-amount (- (car (cdr coordinates)) (car (car coordinates))))
		(y-amount (- (cdr (cdr coordinates)) (cdr (car coordinates)))))
	(let loop ((x-index 0)
			   (y-index 0))
	  (cond
	   ((and (or (> (abs x-index) (abs x-amount)) (zero? x-amount)) (or (> (abs y-index) (abs y-amount)) (zero? y-amount)))
		grid)
	   (else
		(increase-coordinate grid (cons (+ (car (car coordinates)) x-index) (+ (cdr (car coordinates)) y-index)))
		(loop (+ x-index (divide-default-zero x-amount (abs x-amount))) (+ y-index (divide-default-zero y-amount (abs y-amount)))))))))

(define (process-line line)
  (let* ((removed-arrow (string-split (string-delete (lambda (c) (or (eqv? c #\-) (eqv? c #\>))) line) #\space))
		 (cleaned-space (filter (lambda (e) (not (string-null? e))) removed-arrow)))
	(cons (string-numbers->pair-numbers (car cleaned-space) #\,) (string-numbers->pair-numbers (car (cdr cleaned-space)) #\,))))
