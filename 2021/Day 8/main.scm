#!/usr/bin/env -S guile -e entrée -s
!#

(add-to-load-path (string-append (dirname (current-filename)) "/.."))

(use-modules (aoc)
			 (srfi srfi-1))

;;  0
;;5   1
;;  6
;;4   2
;;  3

(define unique-digit-amounts '(2 4 3 7))

(define segments '((0 1 2 3 4 5)
				   (1 2)
				   (0 1 3 4 6)
				   (0 1 2 3 6)
				   (1 2 5 6)
				   (0 2 3 5 6)
				   (0 2 3 4 5 6)
				   (0 1 2)
				   (0 1 2 3 4 5 6)
				   (0 1 2 3 5 6)))

(define (entrée args)
  (let ((data-list (process-input args process-line)))
	(unless (null? data-list)
	  (solve data-list part1 part2))))

(define (part1 data-list)
  (let loop ((digit-lines data-list)
			 (amount-of-unique 0))
	(cond
	 ((null? digit-lines)
	  amount-of-unique)
	 (else
	  (loop (cdr digit-lines) (+ amount-of-unique (count-unique (car digit-lines))))))))

(define (count-unique digit-line)
  (let ((unique-digits (filter (lambda (i) (member (string-length i) unique-digit-amounts)) (cdr digit-line))))
	(length unique-digits)))


(define (part2 data-list)
  (let loop ((digit-lines data-list)
			 (output-value 0))
	(cond
	 ((null? digit-lines)
	  output-value)
	 (else
	  (let* ((digit-syntax (solve-syntax (car (car digit-lines))))
			 (amount (get-final-number digit-syntax (cdr (car digit-lines)))))
		(loop (cdr digit-lines) (+ output-value amount)))))))

(define (get-final-number syntax output-digits)
  (let loop ((output-value 0)
			 (digits output-digits)
			 (multiplier 1000))
	(cond
	 ((null? digits)
	  output-value)
	 (else
	  (loop (+ output-value (* (read-number (car digits) syntax) multiplier)) (cdr digits) (/ multiplier 10))))))

(define (read-number digit syntax)
  (let loop ((digit-segments (string->list digit))
			 (digit-numbers '()))
	(cond
	 ((null? digit-segments)
	  (find-index (sort-list digit-numbers <) segments))
	 (else
	  (let ((segment-index (find-index (string (car digit-segments)) syntax)))
		(loop (cdr digit-segments) (append digit-numbers (list segment-index))))))))

(define (find-index digit digit-list)
  (let loop ((remaining-digits digit-list)
			 (index 0))
	(cond
	 ((null? remaining-digits)
	  0)
	 ((equal? (car remaining-digits) digit)
	  index)
	 (else
	  (loop (cdr remaining-digits) (+ index 1))))))

(define (solve-syntax unique-digits)
  (let ((solved-digits (make-list 7 "")))
	(list-set! solved-digits 3 (solve-mask unique-digits (merge-letters (list-ref unique-digits 1) (list-ref unique-digits 2))))
	(list-set! solved-digits 0 (subtract-letters (list-ref unique-digits 1) (list-ref unique-digits 0)))
	(list-set! solved-digits 4 (subtract-letters (list-ref unique-digits 9) (merge-letters (list-ref unique-digits 1) (list-ref unique-digits 2) (list-ref solved-digits 3))))
	(list-set! solved-digits 6 (solve-mask unique-digits (merge-letters (list-ref unique-digits 1) (list-ref solved-digits 3))))
	(list-set! solved-digits 5 (subtract-letters (list-ref unique-digits 2) (merge-letters (list-ref unique-digits 0) (list-ref solved-digits 6))))
	(list-set! solved-digits 2 (solve-mask unique-digits (subtract-letters (list-ref unique-digits 9) (list-ref unique-digits 0))))
	(list-set! solved-digits 1 (subtract-letters (list-ref unique-digits 0) (list-ref solved-digits 2)))
	solved-digits))

(define (solve-mask unique-digits mask)
  (let loop ((remaining-digits unique-digits))
	(cond
	 ((null? remaining-digits)
	  '())
	 (else
	  (let ((digits (filter (lambda (i) (not (member i (string->list mask)))) (string->list (car remaining-digits)))))
		(if (and (= (length digits) 1) (= (- (string-length (car remaining-digits)) (string-length mask)) 1))
			(list->string digits)
			(loop (cdr remaining-digits))))))))

(define (subtract-letters l1 l2)
  (list->string (filter (lambda (i) (not (member i (string->list l2)))) (string->list l1))))

(define (merge-letters . args)
  (let* ((appended-mask (string-concatenate args))
		 (filtered-mask (list->string (delete-duplicates (string->list appended-mask) equal?))))
	filtered-mask))

(define (process-line line)
  (let* ((delimited-line (string-split line #\|))
		 (signal-patterns (filter (lambda (e) (not (string-null? e))) (string-split (car delimited-line) #\space)))
		 (four-digit (filter (lambda (e) (not (string-null? e))) (string-split (car (cdr delimited-line)) #\space))))
	(cons (sort-list signal-patterns (lambda (p n) (> (string-length n) (string-length p)))) four-digit)))
