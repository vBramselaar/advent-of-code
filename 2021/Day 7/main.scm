#!/usr/bin/env -S guile -e entrée -s
!#

(add-to-load-path (string-append (dirname (current-filename)) "/.."))

(use-modules (aoc)
			 (ice-9 rdelim))

(define (entrée args)
  (let ((data-list (process-input-port args process-line)))
	(unless (null? data-list)
	  (solve data-list part1 part2))))

(define (part1 data-list)
  (let loop ((index 0)
			 (min-fuel (apply + 0 data-list)))
	(let ((new-fuel (apply + 0 (map (lambda (n) (abs (- n index))) data-list))))
	  (if (>= min-fuel new-fuel)
		  (loop (+ index 1) new-fuel)
		  min-fuel))))


(define (part2-calculate value)
  (+ (* 0.5 (expt (abs value) 2)) (* 0.5 (abs value))))

(define (part2 data-list)
  (let loop ((index 0)
			 (min-fuel (apply + 0 (map (lambda (n) (part2-calculate (car data-list))) data-list))))
	(let ((new-fuel (apply + 0 (map (lambda (n) (part2-calculate (- n index))) data-list))))
	  (if (>= min-fuel new-fuel)
		  (loop (+ index 1) new-fuel)
		  min-fuel))))

(define (process-line port)
  (sort-list (string-numbers->list-numbers (read-line port) #\,) >))
