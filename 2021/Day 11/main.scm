#!/usr/bin/env -S guile -e entrée -s
!#

(add-to-load-path (string-append (dirname (current-filename)) "/.."))

(use-modules (aoc)
			 (srfi srfi-1))

(define (entrée args)
  (let ((data-list (process-input args process-line)))
	(unless (null? data-list)
	  (solve data-list part1 part2))))

(define (part1 data-list)
  (let loop ((index 0)
			 (amount-of-flashes 0)
			 (grid data-list))
	(cond
	 ((>= index 100)
	  amount-of-flashes)
	 (else
	  (let* ((increased-grid (increase-all grid 1))
			 (flashed-dumbos (handle-flashes increased-grid)))
		(loop (+ index 1) (+ amount-of-flashes flashed-dumbos) increased-grid))))))

(define (part2 data-list)
  (let loop ((index 0)
			 (amount-of-flashes 0)
			 (grid data-list))
	(cond
	 ((>= amount-of-flashes (length (flatten data-list)))
	  index)
	 (else
	  (let* ((increased-grid (increase-all grid 1))
			 (flashed-dumbos (handle-flashes increased-grid)))
		(loop (+ index 1) flashed-dumbos increased-grid))))))

(define (handle-flashes grid)
  (let ((flashes 0)
		(h (length grid))
		(w (length (car grid))))
	(let row-loop ((y 0)
				   (increase-coordinates '())
				   (flash-coordinates '()))
	  (cond
	   ((>= y h)
		(increase-by-coordinates grid increase-coordinates)
		(flash-by-coordinates grid flash-coordinates)
		(if (= (length flash-coordinates) 0)
			0
			(+ (length flash-coordinates) (handle-flashes grid))))
	   (else
		(let loop ((x 0)
				   (row-increases '())
				   (row-flashes '()))
		  (cond
		   ((>= x w)
			(row-loop (+ y 1) (append increase-coordinates row-increases) (append flash-coordinates row-flashes)))
		   ((check-amount x y grid)
			(loop (+ x 1) (append row-increases (get-increase-coordinates x y grid)) (append row-flashes (list (cons x y)))))
		   ((not (check-amount x y grid))
			(loop (+ x 1) row-increases row-flashes)))))))))

(define (increase-by-coordinates grid increase-coordinates)
  (cond
   ((null? increase-coordinates)
	'())
   (else
	(when (not (= (list-ref (list-ref grid (cdr (car increase-coordinates))) (car (car increase-coordinates))) 0))
	  (increase-point (car (car increase-coordinates)) (cdr (car increase-coordinates)) grid))
	(increase-by-coordinates grid (cdr increase-coordinates)))))

(define (flash-by-coordinates grid flash-coordinates)
  (cond
   ((null? flash-coordinates)
	'())
   (else
	(list-set! (list-ref grid (cdr (car flash-coordinates))) (car (car flash-coordinates)) 0)
	(flash-by-coordinates grid (cdr flash-coordinates)))))


(define (get-increase-coordinates x y grid)
  (let* ((up (- y 1))
		 (down (+ y 1))
		 (grid-length (length (list-ref grid y)))
		 (coordinates '()))
	(when (>= up 0)
	  (when (>= (- x 1) 0)
		(set! coordinates (append coordinates (list (cons (- x 1) up)))))
	  (when (< (+ x 1) grid-length)
		(set! coordinates (append coordinates (list (cons (+ x 1) up)))))
	  (set! coordinates (append coordinates (list (cons x up)))))
	(when (< down (length grid))
	  (when (>= (- x 1) 0)
		(set! coordinates (append coordinates (list (cons (- x 1) down)))))
	  (when (< (+ x 1) grid-length)
		(set! coordinates (append coordinates (list (cons (+ x 1) down)))))
	  (set! coordinates (append coordinates (list (cons x down)))))
	(when (>= (- x 1) 0)
	  (set! coordinates (append coordinates (list (cons (- x 1) y)))))
	(when (< (+ x 1) grid-length)
	  (set! coordinates (append coordinates (list (cons (+ x 1) y)))))
	coordinates))

(define (check-amount x y grid)
  (let ((number (list-ref (list-ref grid y) x)))
	(if (> number 9)
		#t
		#f)))

(define (increase-point x y grid)
  (list-set! (list-ref grid y) x (+ (list-ref (list-ref grid y) x) 1)))

(define (increase-all grid amount)
  (cond
   ((null? grid)
	'())
   (else
	(let loop ((numbers (car grid))
			   (new-numbers '()))
	  (cond
	   ((null? numbers)
		(append (list new-numbers) (increase-all (cdr grid) amount)))
	   (else
		(loop (cdr numbers) (append new-numbers (list (+ (car numbers) 1))))))))))

(define (process-line line)
  (let* ((numbers-line (string->list line))
		 (numbers (map (λ (e) (string->number (string e))) numbers-line)))
	numbers))
