(define-module (aoc)
  #:use-module (ice-9 rdelim)
  #:export (solve)
  #:export (process-input)
  #:export (process-input-port)
  #:export (string-numbers->list-numbers)
  #:export (string-numbers->pair-numbers)
  #:export (flatten))

(define (solve data-list part1 part2)
  (let ((answer (part1 data-list)))
	(display "Answer part 1: ") (display answer) (newline))
  (let ((answer (part2 data-list)))
	(display "Answer part 2: ") (display answer) (newline)))

(define (process-input args processor)
  (cond
   ((and (= (length args) 2) (access? (car (cdr args)) R_OK))
	(call-with-input-file (car (cdr args))
	  (lambda (port)
		(let loop ((new-list '())
				   (line (read-line port)))
		  (if (eof-object? line)
			  new-list
			  (loop (append new-list (list (processor line))) (read-line port)))))))
   (else
	(display "Give valid input file") (newline)
	'())))

(define (process-input-port args processor)
  (cond
   ((and (= (length args) 2) (access? (car (cdr args)) R_OK))
	(call-with-input-file (car (cdr args)) processor))
   (else
	(display "Give valid input file") (newline)
	'())))

(define (string-numbers->list-numbers str delim)
  (let ((string-list (filter (lambda (e) (not (string-null? e))) (string-split str delim))))
	(map (lambda (n) (string->number (string-trim-both n))) string-list)))

(define (string-numbers->pair-numbers str delim)
  (let* ((string-list (filter (lambda (e) (not (string-null? e))) (string-split str delim)))
		 (converted-list (map (lambda (n) (string->number (string-trim-both n))) string-list)))
	(cons (car converted-list) (car (cdr converted-list)))))

(define (flatten x)
  (cond
   ((null? x)
	'())
   ((not (pair? x))
	(list x))
   (else
	(append (flatten (car x)) (flatten (cdr x))))))
