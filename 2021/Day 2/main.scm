#!/usr/bin/env -S guile -e entrée -s
!#

(add-to-load-path (string-append (dirname (current-filename)) "/.."))

(use-modules (aoc))

(define (entrée args)
  (let ((data-list (process-input args process-line)))
	(unless (null? data-list)
	  (solve data-list part1 part2))))

(define (part1 data-list)
  (let loop ((depth 0)
			 (horizontal 0)
			 (movements data-list))
	(if (null? movements)
		(* depth horizontal)
		(let ((command (car (car movements)))
			  (value (cdr (car movements))))
		  (cond
		   ((string=? command "forward")
			(loop depth (+ horizontal value) (cdr movements)))
		   ((string=? command "up")
			(loop (- depth value) horizontal (cdr movements)))
		   ((string=? command "down")
			(loop (+ depth value) horizontal (cdr movements))))))))

(define (part2 data-list)
  (let loop ((aim 0)
			 (depth 0)
			 (horizontal 0)
			 (movements data-list))
	(if (null? movements)
		(* depth horizontal)
		(let ((command (car (car movements)))
			  (value (cdr (car movements))))
		  (cond
		   ((string=? command "forward")
			(loop aim (+ depth (* value aim)) (+ horizontal value) (cdr movements)))
		   ((string=? command "up")
			(loop (- aim value) depth horizontal (cdr movements)))
		   ((string=? command "down")
			(loop (+ aim value) depth horizontal (cdr movements))))))))

(define (process-line line)
  (let ((strings (string-split line #\space)))
	(cons (car strings) (string->number (car (cdr strings))))))
