#include <iostream>
#include <fstream>
#include <string>
#include <iterator>
#include <algorithm>
#include <vector>
#include <deque>

bool addToPreamble(const std::size_t length, int value, std::deque<int>& preamble)
{
	preamble.push_back(value);
	
	if (preamble.size() == length)
	{
		return true;
	}
	else if (preamble.size() > length)
	{
		preamble.pop_front();
	}
	
	return false;
}

bool checkSum(int value, const std::deque<int>& preamble)
{

	for (auto it = preamble.begin(); it != preamble.end(); ++it)
	{
		int valueNeeded = value - *it;
		if(valueNeeded != *it)
		{
			if (std::find(preamble.begin(), preamble.end(), valueNeeded) != preamble.end())
			{
				return true;
			}
		}
	}
	
	return false;
}

int partOne()
{
	int result = 0;
	std::deque<int> preamble;
	const std::size_t preambleLength = 25;
	std::ifstream file("data.txt");

	int number;

	//fill up
	while (file >> number)
	{
		if(addToPreamble(preambleLength, number, preamble))
		{
			break;
		}
	}

	//sumcheck the rest
	while (file >> number)
	{
		if (!checkSum(number, preamble))
		{
			return number;
		}
		
		addToPreamble(preambleLength, number, preamble);
	}

	return result;
}

int findCombi(std::size_t combiSize, int targetNumber, const std::vector<int>& numbers)
{
	std::size_t indexPos = combiSize - 1;
	
	for (std::size_t i = indexPos; i < numbers.size(); i++)
	{
		int numberCheck = 0;
		int highest = numbers[i - indexPos];
		int lowest = numbers[i - indexPos];
		
		for (std::size_t j = i - indexPos; j <= i; j++)
		{
			numberCheck += numbers[j];

			if (highest < numbers[j]) { highest = numbers[j]; }
			if (lowest > numbers[j]) { lowest = numbers[j]; }
		}

		if (numberCheck == targetNumber)
		{
			return lowest + highest;
		}
	}
	
	return -1;
}

int partTwo(int badNumber)
{
	int result = 0;

	std::vector<int> numbers;
	std::ifstream file("data.txt");
	
	std::copy(std::istream_iterator<int>(file), std::istream_iterator<int>(), 
			  std::back_inserter(numbers));

	for (std::size_t i = 2; i < numbers.size(); i++)
	{
		result = findCombi(i, badNumber, numbers);
		if(result != -1)
		{
			break;
		}
	}
	
	return result;
}

int main()
{
	int answer = partOne();
	std::cout << "Answer part one: " << answer << std::endl;

	int answer2 = partTwo(answer);
	std::cout << "Answer part two: " << answer2 << std::endl;
	return 0;
}
