#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <iterator>
#include <cmath>
#include <algorithm>
#include "structs.h"

std::vector<std::string> data;
const int rowDigits = 7;
const int columnDigits = 3;
const int amountOfRows = 127;
const int amountOfColumns = 7;

void loadFile()
{
	std::ifstream file("data.txt");

	std::copy(std::istream_iterator<std::string>(file), std::istream_iterator<std::string>(), 
			  std::back_inserter(data));
}

int parseBinarySpace(const std::string& pass, const ParseSetting& setting)
{
	int result = 0;
	float upper = static_cast<float>(setting.max);
	float lower = 0;

	for(int i = setting.start; i < (setting.start + setting.offset); i++)
	{
		if(pass[i] == setting.upper)
		{
			upper -= round(((upper - lower) * 0.5));
			result = lower;
		}
		else if (pass[i] == setting.lower)
		{
			lower += round(((upper - lower) * 0.5));
			result = lower;
		}
	}

	return result;
}

int partOne()
{

	int result = 0;
	
	for (const std::string& date : data)
	{
		ParseSetting rowSetting = { 0, 7, 127, 'F', 'B' };
		int rows = parseBinarySpace(date, rowSetting);

		ParseSetting columnSetting = { 7, 3, 7, 'L', 'R' };
		int column = parseBinarySpace(date, columnSetting);

		int pass = rows * 8 + column;
		if(pass > result) { result = pass; }
	}
	
	return result;
}

int partTwo()
{
	std::vector<Seat> seats;
	std::vector<Seat> untakenSeats;
	
	int result = 0;

	for (const std::string& date : data)
	{
		Seat seat;
		
		ParseSetting rowSetting = { 0, 7, 127, 'F', 'B' };
		seat.row = parseBinarySpace(date, rowSetting);

		ParseSetting columnSetting = { 7, 3, 7, 'L', 'R' };
		seat.column = parseBinarySpace(date, columnSetting);

		seats.push_back(seat);
	}


	for (int i = 0; i < amountOfRows; i++)
	{
		for (int j = 0; j < amountOfColumns; j++)
		{
			Seat seat = { i, j};
			std::vector<Seat>::iterator it = std::find_if(seats.begin(), seats.end(), Seat(seat));
			if (it == seats.end())
			{
				if(seat.row != 0 && seat.row != amountOfRows)
				{
					untakenSeats.push_back(seat);
				}
				
			}
		}
	}

	for (const Seat& seat : untakenSeats)
	{
		int pass = seat.row * 8 + seat.column;
		bool extra = false;
		bool less = false;
		
		for (const Seat& takenSeat : seats)
		{
			int passTaken = takenSeat.row * 8 + takenSeat.column;
			if(passTaken + 1 == pass)
			{
				less = true;
			}

			if (passTaken - 1 == pass)
			{
				extra = true;
			}
		}

		if(extra && less)
		{
			result = pass;
			break;
		}
	}
	
	return result;
}

int main()
{
	loadFile();
	
	int result = partOne();
	int result2 = partTwo();
	std::cout << "Answer part one: " << result	<< std::endl;
	std::cout << "Answer part two: " << result2 << std::endl;
	return 0;
}
