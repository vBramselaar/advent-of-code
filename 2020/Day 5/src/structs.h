#ifndef BVD_STRUCTS_H
#define BVD_STRUCTS_H

struct ParseSetting
{
	int start = 0;
	int offset = 0;
	int max = 0;
	char upper;
	char lower;
};

struct Seat
{
	int row = 0;
	int column = 0;

	bool operator() (const Seat& s) const
    {
        return s.row == row && s.column == column;
    }
};

#endif
