#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <string>

std::vector<std::string> data;

void loadFile()
{
	std::ifstream file("data.txt");

	std::copy(std::istream_iterator<std::string>(file), std::istream_iterator<std::string>(), 
			  std::back_inserter(data));
}

int getAmountOfTrees(int offsetIncrement, int lineIncrement)
{
	size_t line = 0;
	int offset = 0;
	int amountOfTrees = 0;

	while (line < data.size() - lineIncrement)
	{
		offset = (offset + offsetIncrement) % data[line].length();
		line += lineIncrement;

		if(data[line].at(offset) == '#')
		{
			amountOfTrees++;
		}
	}
	
	return amountOfTrees;
}

int calculatePartOne()
{
	return getAmountOfTrees(3, 1);
}

uint64_t calculatePartTwo()
{
	int slopeArray[][2]	 = { {1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2} };
	uint64_t result = 1;

	int arrayLength = sizeof(slopeArray) / sizeof(slopeArray[0]);
	
	for(int i = 0; i < arrayLength; i++)
	{
		result *= getAmountOfTrees(slopeArray[i][0], slopeArray[i][1]);
	}

	return result;
}

int main()
{
	loadFile();

	int result = calculatePartOne();
	uint64_t result2 = calculatePartTwo();
	std::cout << "Answer part one: " << result	<< std::endl;
	std::cout << "Answer part two: " << result2 << std::endl;
	return 0;
}
