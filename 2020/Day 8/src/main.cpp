#include <iostream>
#include <fstream>
#include <string>
#include <iterator>
#include <algorithm>
#include <unordered_set>
#include <sstream>
#include <vector>

using intLine = std::pair<std::string, int>;

bool runCode(const std::vector<intLine>& data, int& accumulator)
{
	std::unordered_set<std::size_t> ranIndexes;
	accumulator = 0;
	
	std::size_t i = 0;
	while(i < data.size())
	{
		if(data[i].first == "acc")
		{
			accumulator += data[i].second;
			i++;
		}
		else if(data[i].first == "jmp")
		{
			i += data[i].second;
		}
		else if(data[i].first == "nop")
		{
			i++;
		}
		
		if(ranIndexes.find(i) != ranIndexes.end())
		{
			return false;
		}
		
		ranIndexes.insert(i);
	}
	
	return true;
}

int partTwo(std::vector<intLine>& data)
{
	int accumulator = 0;

	for (intLine& date : data)
	{
		if (date.first == "nop")
		{
			date.first = "jmp";
			if (!runCode(data, accumulator))
			{
				date.first = "nop";
			}
			else
			{
				break;
			}
		}
		else if (date.first == "jmp")
		{
			date.first = "nop";
			if (!runCode(data, accumulator))
			{
				date.first = "jmp";
			}
			else
			{
				break;
			}
		}
	}
	
 	return accumulator;
}

int main()
{
	std::ifstream file("data.txt");
	std::vector<intLine> instructions;
	
	std::string line;
	while (std::getline(file, line))
	{
		if (!line.empty())
		{
			std::string id = line.substr(0, line.find(" "));
			int value = std::stoi(line.substr(line.find(" ")));
			instructions.emplace_back(id, value);
		}
	}

	int answer = 0;
	runCode(instructions, answer);
	std::cout << "Answer part one: " << answer << std::endl;
	std::cout << "Answer part two: " << partTwo(instructions) << std::endl;
	return 0;
}
