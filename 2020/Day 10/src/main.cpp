#include <iostream>
#include <fstream>
#include <string>
#include <iterator>
#include <algorithm>
#include <vector>

int partOne()
{
	std::vector<int> numbers;
	std::ifstream file("data.txt");
	
	std::copy(std::istream_iterator<int>(file), std::istream_iterator<int>(), 
			  std::back_inserter(numbers));

	std::sort(numbers.begin(), numbers.end());

	int differences[] = {0, 0, 1};
	differences[numbers[0] - 1]++;

	for (std::size_t i = 1; i < numbers.size(); i++)
	{
		int difference = numbers[i] - numbers[i - 1];
		differences[difference - 1]++;
	}
	
	return differences[0] * differences[2];
}

int partTwo(int badNumber)
{
	int result = 0;
	return result;
}

int main()
{
	int answer = partOne();
	std::cout << "Answer part one: " << answer << std::endl;

	int answer2 = partTwo(answer);
	std::cout << "Answer part two: " << answer2 << std::endl;
	return 0;
}
