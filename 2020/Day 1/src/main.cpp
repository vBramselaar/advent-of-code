#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>


int partOne()
{
	std::ifstream file("data.txt");
	std::vector<int> data;

	std::copy(std::istream_iterator<int>(file), std::istream_iterator<int>(), 
			  std::back_inserter(data));

	if (!file.eof())
	{
		std::cout << "Didn't reach end of file" << std::endl;
		return -1;
	}

	for (int date : data)
	{
		for (int comparer : data)
		{
			if((date + comparer) == 2020)
			{
				std::cout << "Result: " << (date * comparer) << std::endl;
				return 0;
			}
		}
	}

	return 0;
}

int partTwo()
{
	std::ifstream file("data.txt");
	std::vector<int> data;

	std::copy(std::istream_iterator<int>(file), std::istream_iterator<int>(), 
			  std::back_inserter(data));

	if (!file.eof())
	{
		std::cout << "Didn't reach end of file" << std::endl;
		return -1;
	}

	for (int date : data)
	{
		for (int firstComparer : data)
		{
			for (int secondComparer : data)
			{
				if ((date + firstComparer + secondComparer) == 2020)
				{
					std::cout << "Result: " << (date * firstComparer * secondComparer) << std::endl;
					return 0;
				}
			}
		}
	}

	return 0;
}

int main()
{
	return partTwo();
}
