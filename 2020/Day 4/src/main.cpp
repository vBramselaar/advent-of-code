#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include "values.h"

std::vector<std::string> data;

const std::string codes[] = { "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" };

const Checker checkers[] = { {"byr", "", 1920, 2002}, { "iyr", "", 2010, 2020 },
							 { "eyr", "", 2020, 2030 }, { "hgt", "cm", 150, 193 },
							 { "hgt", "in", 59, 76 }, { "hcl", "#", 6, 6 },
							 { "ecl", "amb blu brn gry grn hzl oth", 0, 0}, { "pid", "", 9, 9 } };

bool isValidPartOne(const std::string& data)
{
	for (const std::string& code : codes)
	{
		if (data.find(code + ":") == std::string::npos)
		{
			return false;
		}
	}

	return true;
}

bool minMaxCheck(const Checker checker, const std::string& data)
{
	try
	{
		int number = std::stoi(data);
		if(number < checker.min || number > checker.max)
		{
			return false;
		}
	}
	catch (const std::invalid_argument& e)
	{
		return false;
	}

	return true;
}

bool checkerCheck(const Checker checker, const std::string& data)
{
	if(checker.id == "pid")
	{
		if(data.length() != static_cast<size_t>(checker.min))
		{
			return false;
		}
		
		return std::all_of(data.begin(), data.end(), ::isdigit);
	}
	else if(checker.id == "ecl")
	{
		std::stringstream s(checker.subcat);
		std::string word;
		bool result = false;
		while(s >> word)
		{
			if(word == data) { result = true; }
		}
		return result;
	}
	else if(checker.id == "hcl")
	{
		if(checker.subcat[0] != data[0] ||
		   static_cast<size_t>(checker.min) != data.length() - 1)
		{
			return false;
		}

		try
		{
			std::stoi(data.substr(1), 0, 16);
		}
		catch (const std::invalid_argument& e)
		{
			return false;
		}
	}
	else if(checker.id == "hgt")
	{
		if (data.substr(data.length() - 2) != "cm" && data.substr(data.length() - 2) != "in")
		{
			return false;
		}
		else if(data.substr(data.length() - 2) == checker.subcat)
		{
			return minMaxCheck(checker, data.substr(0, data.length() - 2));
		}
	}
	else //byr iyr eyr
	{
		return minMaxCheck(checker, data);
	}
	return true;
}

bool isValidPartTwo(const std::string& data)
{
	for (const Checker& checker : checkers)
	{
		std::string key = checker.id + ":";
		std::size_t pos = data.find(key);
		if (pos == std::string::npos)
		{
			return false;
		}

		bool check = checkerCheck(checker, data.substr(pos + key.length(), data.substr(pos + key.length()).find(" ")));
		if(!check)
		{
			return false;
		}
	}

	return true;
}

int calculate(bool part)
{
	std::ifstream file("data.txt");
	std::string line;
	std::string temp;
	int validPasswords = 0;
	
	while (std::getline(file, line))
	{
		if (line.empty())
		{
			if (part)
			{
				validPasswords += static_cast<int>(isValidPartOne(temp));
			}
			else
			{
				validPasswords += static_cast<int>(isValidPartTwo(temp));

			}
			temp.clear();
		}
		else
		{
			temp += line + " ";
		}
	}

	if (part)
	{
		validPasswords += static_cast<int>(isValidPartOne(temp));
	}
	else
	{
		validPasswords += static_cast<int>(isValidPartTwo(temp));
	}

	return validPasswords;
}

int main()
{
	int result = calculate(true);
	int result2 = calculate(false);
	std::cout << "Answer part one: " << result	<< std::endl;
	std::cout << "Answer part two: " << result2 << std::endl;
	return 0;
}
