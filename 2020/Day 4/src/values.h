#ifndef BVD_VALUES_H
#define BVD_VALUES_H

#include <string>

struct Checker
{
	std::string id;
	std::string subcat;
	int min=0;
	int max=0;
};

#endif
