#include <iostream>
#include <fstream>
#include <string>
#include <iterator>
#include <algorithm>
#include <unordered_map>
#include <sstream>


int countUniqueUsingHash(const std::string& s) 
{ 
	std::unordered_map<char, std::size_t> counter; 

	for (std::size_t i = 0; i < s.length(); i++)
	{
		counter.insert({s[i], i});
	} 

	return counter.size(); 
}

int partOne()
{
	int result = 0;

	std::ifstream file("data.txt");
	
	std::string line;
	std::string temp;
	while (std::getline(file, line))
	{
		if (line.empty())
		{
			result += countUniqueUsingHash(temp);
			temp.clear();
		}
		else
		{
			temp += line;
		}
	}

	if(!temp.empty())
	{
		result += countUniqueUsingHash(temp);
	}

	return result;
}

int countAllPresent(const std::string& group) 
{
	std::stringstream s(group);

	std::string resultS;
	std::getline(s, resultS, ' ');
	
	std::string person;
	while (s >> person)
	{
		std::string temp;
		for (char c : person)
		{
			std::size_t pos = resultS.find(c);
			if (pos != std::string::npos)
			{
				temp += c;
			}
		}
		resultS = temp;
	}

	return resultS.length(); 
}

int partTwo()
{
	int result = 0;

	std::ifstream file("data.txt");
	
	std::string line;
	std::string temp;
	while (std::getline(file, line))
	{
		if (line.empty())
		{
			int test = countAllPresent(temp);
			result += test;
			temp.clear();
		}
		else
		{
			temp += line + " ";
		}
	}

	if(!temp.empty())
	{
		result += countAllPresent(temp);
	}

	return result;
}

int main()
{
	std::cout << "Answer part one: " << partOne() << std::endl;
	std::cout << "Answer part two: " << partTwo() << std::endl;
	return 0;
}
