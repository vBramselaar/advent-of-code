#include <iostream>
#include <fstream>
#include <string>
#include <iterator>
#include <algorithm>
#include <unordered_map>
#include <sstream>
#include <vector>


const std::string bagDivider = "contain ";

int findHowManyHaveShiny(const std::unordered_map<std::string, std::string>& bagRules, std::string bag)
{
	if(bag == "no other bags")
	{
		return 0;
	}

	std::stringstream s(bag);
	std::string bagType;
	
	while (std::getline(s, bagType, ','))
	{
		if(bagType[0] == ' ')
		{
			bagType.erase(0, 1);
		}
		
		std::string type = bagType.substr(bagType.find(' ') + 1, bagType.find_last_of(' ') - 2);
		
		if (type == "shiny gold")
		{
			return 1;
		}
		else if (findHowManyHaveShiny(bagRules, bagRules.find(type)->second) >= 1) // if no shiny, search deeper
		{
			return 1;
		}
	}
	
	return 0;
}

int findHowManyBags(const std::unordered_map<std::string, std::string>& bagRules, std::string bag)
{
	int result = 0;
	
	if(bag == "no other bags")
	{
		return result;
	}

	std::stringstream s(bag);
	std::string bagType;
	
	while (std::getline(s, bagType, ','))
	{
		if(bagType[0] == ' ')
		{
			bagType.erase(0, 1);
		}

		int amount = std::stoi(bagType.substr(0, bagType.find(' ')));
		std::string type = bagType.substr(bagType.find(' ') + 1, bagType.find_last_of(' ') - 2);

		result += amount;
		result += amount * findHowManyBags(bagRules, bagRules.find(type)->second);
	}
	
	return result;
}

int partOne(const std::unordered_map<std::string, std::string>& data)
{
	int result = 0;

	for (std::pair<std::string, std::string> bag : data)
	{
		result += findHowManyHaveShiny(data, bag.second);
	}

	return result;
}

int partTwo(const std::unordered_map<std::string, std::string>& data)
{
	int result = 0;

	result += findHowManyBags(data, data.find("shiny gold")->second);
	
	return result;
}

int main()
{
	std::ifstream file("data.txt");
	std::unordered_map<std::string, std::string> bagRules;
	
	std::string line;
	while (std::getline(file, line))
	{
		if (!line.empty())
		{
			std::string bag = line.substr(0, line.find("bags") - 1);
			std::size_t seperator = line.find(bagDivider) + bagDivider.length();
			std::string containingBags = line.substr(seperator, line.find('.') - seperator);

			bagRules.emplace(bag, containingBags);
		}
	}
	
	std::cout << "Answer part one: " << partOne(bagRules) << std::endl;
	std::cout << "Answer part two: " << partTwo(bagRules) << std::endl;
	return 0;
}
