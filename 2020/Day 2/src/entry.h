#ifndef BVD_ENTRY_H
#define BVD_ENTRY_H

#include <string>

struct Entry
{
	int min = 0;
	int max = 0;
	char c = '\0';
	std::string password;
};

#endif
