#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include "entry.h"

std::vector<Entry> data;

void loadFile()
{
	std::ifstream file("data.txt");
	std::string line;
	while (std::getline(file, line))
	{
		Entry entry;
		
		entry.min = std::stoi(line.substr(0, line.find("-")) , nullptr);
		entry.max = std::stoi(line.substr(line.find("-") + 1, line.find(" ")) , nullptr);
		entry.c = line.at(line.find(" ") + 1);
		entry.password = line.substr(line.find(": ") + 2);

		std::cout << entry.min	<< "-" << entry.max << " " << entry.c << ": " << entry.password << std::endl;

		data.push_back(entry);
	}
}

int calculatePartOne()
{
	int amountOfValid = 0;
	for(Entry entry : data)
	{
		int charCount = 0;
		for(char c : entry.password)
		{
			if(c == entry.c)
			{
				charCount++;
			}
		}

		if(charCount >= entry.min && charCount <= entry.max)
		{
			amountOfValid++;
		}
	}
	
	return amountOfValid;
}

int calculatePartTwo()
{
	int amountOfValid = 0;
	for(Entry entry : data)
	{
		if((entry.password[entry.min - 1] == entry.c && entry.password[entry.max - 1] != entry.c) ||
		   (entry.password[entry.min - 1] != entry.c && entry.password[entry.max - 1] == entry.c))
		{
			amountOfValid++;
		}
	}
	
	return amountOfValid;
}

int main()
{
	loadFile();

	std::cout << "Answer: " << calculatePartTwo() << std::endl;
	return 0;
}
