#include "grid.hpp"
#include "split.hpp"
#include <iostream>

int main(void)
{
	bvd::row_t<int> rowOfOnes(60,1);
	bvd::Grid<int> grid(60, rowOfOnes);

	std::cout << grid << std::endl;


	std::string text = "5,10,20,40,4000";
	std::vector<int> results;
	bvd::split(results, text, ',');

	std::for_each(results.begin(), results.end(), [](const auto& n) { std::cout << n << " "; });
	std::cout << std::endl;

	std::string text2 = "5, 10, 20, 40, 4000";
	std::vector<int> results2;
	bvd::split(results2, text2, ", ");

	std::for_each(results2.begin(), results2.end(), [](const auto& n) { std::cout << n << " "; });
	std::cout << std::endl;
	
	return 0;
}
