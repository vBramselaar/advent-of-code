#ifndef BVD_SPLIT_H
#define BVD_SPLIT_H

#include <vector>
#include <sstream>
#include <algorithm>
#include <functional>

namespace bvd
{
	template <typename T>
	void split(std::vector<T>& results, const std::string& text, char delim)
	{
		std::string token;
		std::istringstream tokenStream(text);
		while (std::getline(tokenStream, token, delim))
		{
			T value;
			std::istringstream iss(token);
			iss >> value;
			results.push_back(value);
		}
	}

	template <typename T>
	void split(std::vector<T>& results, const std::string& text, const std::string& delim)
	{
		size_t last_pos = 0;
		for (size_t pos = text.find(delim); pos != std::string::npos; pos = text.find(delim, last_pos))
		{
			T value;
			std::istringstream iss(text.substr(last_pos, pos));
			iss >> value;
			results.push_back(value);

			last_pos = pos + delim.length();
		}

		T value;
		std::istringstream iss(text.substr(last_pos));
		iss >> value;
		results.push_back(value);
	}
}

#endif
