#ifndef BVD_GRID_H
#define BVD_GRID_H

#include <vector>
#include <ostream>
#include <algorithm>
#include <iterator>

#define PRINT_SIZE 50

namespace bvd
{
	template<class T>
	using row_t = std::vector<T>;

	template<class T>
	class Grid
	{
	public:
		Grid() = default;
		Grid(size_t amount, const row_t<T>& row)
		{
			grid = row_t<row_t<T>>(amount, row);
		}
		
		Grid(size_t w, size_t h, T init)
		{
			grid = row_t<row_t<T>>(row_t<T>(w, row_t<T>(h, init)));
		}

		void push_back_row(const row_t<T>& row)
		{
			grid.push_back(row);
		}

		void pop_back_row()
		{
			grid.pop_back();
		}

		void push_back_item(size_t row_index, const T& item)
		{
			grid.at(row_index).push_back(item);
		}

		void pop_back_item(size_t row_index)
		{
			grid.at(row_index).pop_back();
		}

		std::pair<size_t,size_t> size() const
		{
			if (grid.size() == 0)
			{
				return {grid.size(), 0};
			}
			else
			{
				return {grid.size(), grid.at(0).size()};
			}
		}

		T& at(size_t x, size_t y)
		{
			return grid.at(x).at(y);
		}
		const T& at(size_t x, size_t y) const
		{
			return grid.at(x).at(y);
		}

		row_t<T>& row_at(size_t x)
		{
			return grid.at(x);
		}
		const row_t<T>& row_at(size_t x) const
		{
			return grid.at(x);
		}

		// std::cout << Grid<T> << std::endl;
		friend std::ostream& operator<<(std::ostream& os, const Grid<T>& obj)
		{
			std::pair<size_t,size_t> size = obj.size();
			os << "Size: " << size.first << " x " << size.second << std::endl;

			size_t xPrintSize = size.first;
			std::string xEndingPrint = "";
			if (size.first > PRINT_SIZE)
			{
				xPrintSize = PRINT_SIZE;
				xEndingPrint = "...";
			}
			
			for (size_t i = 0; i < xPrintSize; i++)
			{
				size_t yPrintSize = size.second;
				std::string yEndingPrint = "";
				if (size.second > PRINT_SIZE)
				{
					yPrintSize = PRINT_SIZE;
					yEndingPrint = "...";
				}
				
				std::copy(obj.row_at(i).begin(), obj.row_at(i).begin() + yPrintSize, std::ostream_iterator<T>(os, " "));
				os << yEndingPrint << std::endl;
			}
			os << xEndingPrint;
			
			return os;
		}

		// for (const bvd::row_t<int>& row : grid)
		class Iterator
		{
		public:
			using iterator_category = std::forward_iterator_tag;
			using difference_type	= std::ptrdiff_t;
			using value_type		= row_t<T>;
			using pointer			= value_type*;
			using reference			= value_type&;

			Iterator(row_t<row_t<T>>& newGrid) : grid(newGrid), index(0) {}
			Iterator(row_t<row_t<T>>& newGrid, size_t index) : grid(newGrid), index(index) {}
			
			reference operator*() const { return grid.at(index); }
			pointer operator->() { return &grid.at(index); }

			Iterator& operator++()
			{
				index++;
				return *this;
			}  
			Iterator operator++(int) { Iterator tmp = *this; ++index; return tmp; }

			friend bool operator== (const Iterator& a, const Iterator& b) { return a.index == b.index; };
			friend bool operator!= (const Iterator& a, const Iterator& b) { return a.index != b.index; };	  

		private:
			row_t<row_t<T>>& grid;
			size_t index;
		};
		Iterator begin() { return Iterator(grid); }
		Iterator end()	 { return Iterator(grid, grid.size()); }
		
	private:
		row_t<row_t<T>> grid;
	};
}

#endif
